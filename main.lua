local mainCanvasW = 160
local mainCanvasH = 144
local mainCanvas = love.graphics.newCanvas(mainCanvasW, mainCanvasH)
local background = love.graphics.newImage("background.jpg")

local Game = require("game")
local game = Game:new()

function love.load()
   love.window.setMode(mainCanvasW*4, mainCanvasH*4, {
      resizable = true,
      minwidth = mainCanvasW,
      minheight = mainCanvasH,
   })
   love.graphics.setBackgroundColor(80/255, 0, 0)

   mainCanvas:setFilter("nearest", "nearest", 0)
   game:load()
end

function love.update(dt)
   if love.keyboard.isDown("escape") then
      love.event.quit(0)
      return
   end

   game:update(dt)
end

function love.draw()
   love.graphics.setCanvas(mainCanvas)
   game:draw()
   love.graphics.setCanvas()
   love.graphics.setColor(1, 1, 1)

   -- blit mainCanvas to screen with scaling

   local windowW, windowH = love.graphics.getDimensions()
   local backgroundW, backgroundH = background:getDimensions()
   local backgroundScale = math.max(
      windowW / backgroundW,
      windowH / backgroundH
   )
   local mainCanvasScale = math.floor(math.min(
      windowW / mainCanvasW,
      windowH / mainCanvasH
   ))

   love.graphics.draw(
      background,
      (windowW - backgroundW*backgroundScale) / 2,
      (windowH - backgroundH*backgroundScale) / 2,
      0,
      backgroundScale,
      backgroundScale
   )
   love.graphics.draw(
      mainCanvas,
      (windowW - mainCanvasW*mainCanvasScale) / 2,
      (windowH - mainCanvasH*mainCanvasScale) / 2,
      0,
      mainCanvasScale,
      mainCanvasScale
   )
end
