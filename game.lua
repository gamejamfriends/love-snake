local canvasW = 160
local canvasH = 144
local squaresX = 17
local squaresY = 14
local offsetX = 8
local offsetY = 8
local squareSizeX = 8
local squareSizeY = 8

local bgColor = { r = 0, g = 0, b = 60/255 }
local boardColor1 = { r = 0, g = 0, b = 80/255 }
local boardColor2 = { r = 0, g = 0, b = 100/255 }
local snakeColor = { r = 0, g = 120/255, b = 0 }
local appleColor = { r = 160/255, g = 0, b = 0 }
local scoreColor = { r = 160/255, g = 160/255, b = 0 }

local font = love.graphics.newFont("terminus.ttf", nil, "none")

function calculateNewPosition(pos, direction)
   ret = { x = pos.x, y = pos.y }

   if (direction == "right") then
      ret.x = ret.x + 1
      if (ret.x > squaresX) then
         ret.x = 0
      end
   elseif (direction == "down") then
      ret.y = ret.y + 1
      if (ret.y > squaresY) then
         ret.y = 0
      end
   elseif (direction == "left") then
      ret.x = ret.x - 1
      if (ret.x < 0) then
         ret.x = squaresX
      end
   elseif (direction == "up") then
      ret.y = ret.y - 1
      if (ret.y < 0) then
         ret.y = squaresY
      end
   end

   return ret
end

function moveSnake(snake, pos, growSnake)
   newSnake = { pos }
   for i, snakePoint in ipairs(snake) do
      newSnake[i+1] = snakePoint
   end

   if (not growSnake) then
      table.remove(newSnake)
   end
   return newSnake
end

function findApple(pos, apples)
   for i, apple in ipairs(apples) do
      if (apple.x == pos.x and apple.y == pos.y) then
         return apple
      end
   end

   return nil
end

function isSnake(pos, snake)
   for _, snakePoint in ipairs(snake) do
      if (snakePoint.x == pos.x and snakePoint.y == pos.y) then
         return true
      end
   end

   return false
end

function removeApple(find, apples)
   for i, apple in ipairs(apples) do
      if (apple.x == find.x and apple.y == find.y) then
         table.remove(apples, i)
         return
      end
   end
end

local Game = {}

-- canvas MUST be 160x144
function Game:new()
   o = {
      score = 0,
      snake = {{x = 4, y = 3}, {x = 3, y = 3}, {x = 2, y = 3}},
      apples = {{x = 10, y = 10, lifetime = 20}},
      direction = "right",
      moveTime = 0.15,
      timeSinceMove = 0.0,
      gameOver = false,
      lastMove = "right"
   }

   setmetatable(o, self)
   self.__index = self
   return o
end

function Game:load()
end

function Game:update(dt)
   if (dt == nil) then return end
   if (self.gameOver) then return end

   self.timeSinceMove = self.timeSinceMove + dt

   local newPos
   while (self.timeSinceMove > self.moveTime) do
      newPos = calculateNewPosition(self.snake[1], self.direction)

      if (isSnake(newPos, self.snake)) then
         self.gameOver = true
         return
      end

      local growSnake = false
      local apple = findApple(newPos, self.apples)
      if (apple ~= nil) then
         removeApple(apple, self.apples)
         self.score = self.score + math.ceil(apple.lifetime)
         growSnake = true
      end
      self.snake = moveSnake(self.snake, newPos, growSnake)
      self.lastMove = self.direction
      self.timeSinceMove = self.timeSinceMove - self.moveTime
   end

   for _, apple in ipairs(self.apples) do
      apple.lifetime = apple.lifetime - dt
      if (apple.lifetime <= 0) then
         removeApple(apple, self.apples)
      end
   end

   if (#(self.apples) == 0) then
      table.insert(self.apples, self:newApple(newPos))
   end

   if love.keyboard.isDown("w") or love.keyboard.isDown("up") then
      if (self.lastMove ~= "down") then
         self.direction = "up"
      end
   elseif love.keyboard.isDown("a") or love.keyboard.isDown("left") then
      if (self.lastMove ~= "right") then
         self.direction = "left"
      end
   elseif love.keyboard.isDown("s") or love.keyboard.isDown("down") then
      if (self.lastMove ~= "up") then
         self.direction = "down"
      end
   elseif love.keyboard.isDown("d") or love.keyboard.isDown("right") then
      if (self.lastMove ~= "left") then
         self.direction = "right"
      end
   end
end

function Game:draw()
   if (self.gameover) then return end

   love.graphics.setColor(bgColor.r, bgColor.g, bgColor.b)
   love.graphics.rectangle("fill", 0, 0, canvasW, canvasH)

   for x = 0, squaresX, 1 do
      for y = 0, squaresY, 1 do
         local color
         if ((x+y) % 2 == 0) then
            color = boardColor1
         else
            color = boardColor2
         end

         love.graphics.setColor(color.r, color.g, color.b)
         love.graphics.rectangle(
            "fill",
            offsetX + squareSizeX*x,
            offsetY + squareSizeY*y,
            squareSizeX,
            squareSizeY
         )
      end
   end

   for _, snakePoint in ipairs(self.snake) do
      love.graphics.setColor(snakeColor.r, snakeColor.g, snakeColor.b)
      love.graphics.rectangle(
         "fill",
         1 + offsetX + squareSizeX*snakePoint.x,
         1 + offsetY + squareSizeY*snakePoint.y,
         squareSizeX - 2,
         squareSizeY - 2
      )
   end

   for _, applePoint in ipairs(self.apples) do
      love.graphics.setColor(appleColor.r, appleColor.g, appleColor.b)
      love.graphics.rectangle(
         "fill",
         1 + offsetX + squareSizeX*applePoint.x,
         1 + offsetY + squareSizeY*applePoint.y,
         squareSizeX - 2,
         squareSizeY - 2
      )
   end

   love.graphics.setColor(scoreColor.r, scoreColor.g, scoreColor.b)
   local scoreText = love.graphics.newText(
      font,
      string.format("Score: %08d", self.score)
   )
   local scoreW, scoreH = scoreText:getDimensions()
   love.graphics.draw(
      scoreText,
      (canvasW - scoreW) / 2,
      canvasH - scoreH
   )
end

function Game:newApple(newPos)
   while (true) do
      ::continue::
      local newApple = {
         x = love.math.random(squaresX),
         y = love.math.random(squaresY),
         lifetime = 20
      }

      if (newPos ~= nil) then
         if (newPos.x == newApple.x and newPos.y == newApple.y) then
            goto continue
         end
      end
      if (isSnake(newApple, self.snake)) then goto continue end
      if (findApple(newApple, self.apples) ~= nil) then goto continue end
      return newApple
   end
end

return Game
